<?php
require 'config.php';
// require 'accueilAdmin.php';

try {
    // Essaye de se connecter avec PDO
    $connexion = new PDO("mysql:host=localhost;dbname=TP7;port=3306;charset=utf8", $user, $Mdp);
    echo 'connexion DB etablie';
} catch (PDOException $e) {
    // Stop le script et envoie une erreur si la connexion à échoué
    throw new PDOException($e->getMessage(), (int)$e->getCode());
}

$idCibleCode = $_GET["code"];

$deleteCible = $connexion->prepare("DELETE FROM Cible WHERE Cible_Code = :idCibleCode");
$delCible= $deleteCible->bindValue(':idCibleCode', $idCibleCode, PDO::PARAM_INT);
$delCible= $deleteCible->execute();




header("Location: $url");
?>