<?php

require 'config.php';
    
try {
    // Essaye de se connecter avec PDO
    $connexion = new PDO("mysql:host=localhost;dbname=TP7;port=3306;charset=utf8", $user, $Mdp);
    echo 'connexion DB etablie';
} catch (PDOException $e) {
    // Stop le script et envoie une erreur si la connexion à échoué
    throw new PDOException($e->getMessage(), (int)$e->getCode());
}


// session_start();

$jointureMissionCible=$connexion->prepare ("SELECT Cible_Nom FROM Mission NATURAL JOIN Mission_Cible NATURAL JOIN Cible" );

$jointureMissionCible->execute(array());

$datas= $jointureMissionCible->fetch();

 $_SESSION['cible'] = $datas['Cible_Nom'];

?>