<?php
    require 'config.php';
    require 'jointure.php';
    require 'jointure_cible.php';
    //OUVRIR LA SESSION
    session_start();

    try {
        // Essaye de se connecter avec PDO
        $connexion = new PDO("mysql:host=localhost;dbname=TP7;port=3306;charset=utf8", $user, $Mdp);
        
        echo 'connexion DB etablie';
    } catch (PDOException $e) {
        // Stop le script et envoie une erreur si la connexion à échoué
        throw new PDOException($e->getMessage(), (int)$e->getCode());
    }
    ?>

<div class="row align-items-baseline pl-5">
                                <h6>Agent:</h6>
                                <p class="ml-3"><?= $_SESSION['agentnom']?></p>
                            </div>
                            <div class="row align-items-baseline pl-5">
                                <h6>Cible:</h6>
                               
                                <p class="ml-3"><?= $_SESSION["cible"] ?></p>
                            </div>