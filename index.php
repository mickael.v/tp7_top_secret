<?php
    require 'config.php';

    //CONNECTION A LA BDD
    try {
        // Essaye de se connecter avec PDO
        $connexion = new PDO("mysql:host=localhost;dbname=TP7;port=3306;charset=utf8", $user, $Mdp);
        
       
    } catch (PDOException $e) {
        // Stop le script et envoie une erreur si la connexion à échoué
        throw new PDOException($e->getMessage(), (int)$e->getCode());
    }

    //récupère tout le contenu de la table mission
    $req_mission=$connexion->query('SELECT * FROM Mission');

    //récupère tout le contenu de la table agent
    $req_agent=$connexion->query('SELECT * FROM Agent');

    //récupère tout le contenu de la table cible
    $req_cible=$connexion->query('SELECT * FROM Cible');

    //récupère tout le contenu de la table contact
    $req_contact=$connexion->query('SELECT * FROM Contact');

    //récupère tout le contenu de la table cible
    $req_planque=$connexion->query('SELECT * FROM Planque');

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>M&M Agency - Accueil</title>

    <!--Bootstrap-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <!--Mon CSS-->
    <link rel="stylesheet" href="./dist/prod.css">
    <link rel="stylesheet" href="./dist/animation.css">
</head>

<body>
    <header class="container-fluid ">

        <nav class=" d-flex bd-highlight pb-4 navbar navbar-expand-lg navbar-dark">
            <a class="flex-md-grow-1 navbar-brand mr-auto p-2 bd-highlight" href="#">
                <svg width="50" height="50" class="d-inline-block align-top" alt="Logo M&M Agency" loading="lazy"  viewBox="-44 0 512 512.00002" xmlns="http://www.w3.org/2000/svg">
                    <style type="text/css">
                        .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
                        .st2{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
                        .st3{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
                        .st4{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
                        .st5{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
                    </style>
                    <path class="st1" d="m400.675781 219.242188h-11.058593l-30.875-146.214844c-4.34375-20.585938-15.738282-39.222656-32.082032-52.476563-16.34375-13.253906-36.933594-20.550781-57.972656-20.550781-3.03125 0-5.898438 1.355469-7.820312 3.699219-12.125 14.765625-30.035157 23.378906-49.140626 23.628906-.28125.003906-.566406.003906-.847656.003906-18.792968 0-36.570312-8.113281-48.886718-22.34375l-1.289063-1.492187c-1.925781-2.21875-4.71875-3.496094-7.652344-3.496094-44.101562 0-82.121093 31.4375-90.40625 74.753906l-27.632812 144.488282h-10.894531c-13.300782 0-24.117188 10.816406-24.117188 24.113281v27.035156c0 13.300781 10.816406 24.117187 24.117188 24.117187h12.832031c.449219 36.921876 30.609375 66.820313 67.632812 66.820313h9.886719c37.027344 0 67.183594-29.898437 67.632812-66.820313h62.863282c.449218 36.921876 30.609375 66.820313 67.632812 66.820313h9.886719c37.023437 0 67.183594-29.898437 67.632813-66.820313h10.558593c13.296875 0 24.117188-10.816406 24.117188-24.117187v-27.035156c0-13.296875-10.816407-24.113281-24.117188-24.113281zm-341.765625-17.207032h228.609375c5.585938 0 10.117188-4.53125 10.117188-10.117187 0-5.589844-4.53125-10.121094-10.117188-10.121094h-224.742187l19.746094-103.242187c6.179687-32.324219 33.597656-56.175782 66.085937-58.179688 16.320313 17.625 39.269531 27.523438 63.382813 27.1875 23.378906-.304688 45.394531-10.148438 61.167968-27.183594 31.851563 2.003906 59.125 25.285156 65.785156 56.828125l29.988282 142.035157h-313.316406zm55.558594 139.054688h-9.886719c-25.867187 0-46.960937-20.820313-47.410156-46.582032h104.710937c-.449218 25.761719-21.546874 46.582032-47.414062 46.582032zm208.015625 0h-9.886719c-25.867187 0-46.960937-20.820313-47.410156-46.582032h104.710938c-.449219 25.761719-21.546876 46.582032-47.414063 46.582032zm82.074219-70.699219c0 2.140625-1.742188 3.878906-3.878906 3.878906h-376.5625c-2.140626 0-3.878907-1.738281-3.878907-3.878906v-27.035156c0-2.136719 1.742188-3.878907 3.878907-3.878907h376.5625c2.136718 0 3.878906 1.742188 3.878906 3.878907zm0 0"/><path class="st2" d="m221.6875 350.417969c2.214844-5.128907-.144531-11.085938-5.277344-13.300781-5.128906-2.21875-11.085937.144531-13.300781 5.273437l-31.023437 71.792969c-3.4375 7.945312-2.648438 17.011718 2.105468 24.246094 4.753906 7.234374 12.765625 11.554687 21.421875 11.554687h2.355469c5.589844 0 10.117188-4.53125 10.117188-10.117187 0-5.589844-4.527344-10.117188-10.117188-10.117188h-2.355469c-2.628906 0-4.027343-1.703125-4.511719-2.433594-.480468-.730468-1.484374-2.691406-.441406-5.105468zm0 0"/><path class="st3" d="m242.53125 491.761719h-60.269531c-5.589844 0-10.117188 4.53125-10.117188 10.121093 0 5.585938 4.527344 10.117188 10.117188 10.117188h60.269531c5.589844 0 10.117188-4.53125 10.117188-10.117188 0-5.589843-4.527344-10.121093-10.117188-10.121093zm0 0"/><path class="st4" d="m336.605469 187.9375c-2.09375-5.179688-7.992188-7.683594-13.175781-5.585938-5.179688 2.09375-7.679688 7.992188-5.582032 13.175782l.148438.367187c1.589844 3.9375 5.378906 6.328125 9.382812 6.328125 1.261719 0 2.546875-.238281 3.789063-.742187 5.179687-2.097657 7.683593-7.996094 5.585937-13.175781zm0 0"/><path class="st5" d="m237.847656 450.09375c1.265625 0 2.546875-.234375 3.792969-.738281 5.179687-2.097657 7.679687-7.996094 5.585937-13.175781l-.148437-.367188c-2.097656-5.179688-7.996094-7.683594-13.175781-5.585938-5.183594 2.09375-7.683594 7.992188-5.585938 13.175782l.148438.367187c1.59375 3.9375 5.382812 6.324219 9.382812 6.324219zm0 0"/>
                </svg>
                <h1 class="d-inline-block align-top">M&M Agency</h1>
            </a>
            <audio controls="controls">
  <source src="Mission-Impossible.mp3" type="audio/mp3" />
  Votre navigateur n'est pas compatible
</audio>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse p-2 bd-highlight justify-content-end" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item active mr-2">
                        <a class="nav-link" href="#missions">Les missions<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item mr-2">
                        <button type="button" class="btn btn-light">
                            <a href="connexion.php">Me connecter</a>
                        </button>
                    </li>
                </ul>
            </div>
        </nav>

        <div id="focus-container" class="containerAnimation">
            <div class="left-side"></div>
            <div class="right-side"></div>
            <div class="hover">
                <div class="tri-1">
                    <div class="bit-top"></div>
                    <div class="bit-top-left"></div>
                </div>
                
                <div class="tri-2">
                    <div class="bit-top-right"></div>
                    <div class="bit-top-right-2"></div>
                </div>
            
                <div class="tri-3">
                    <div class="bit-bottom-left"></div>
                    <div class="bit-bottom-left-2"></div>
                </div>
            
                <div class="tri-4">
                    <div class="bit-bottom-right"></div>
                    <div class="bit-bottom-right-2"></div>
                </div>
            
            </div>
            <q class="blur">James Bond 007 ? Tu parles d'un agent secret tout le monde le connaît.</q>
            <q class="focus">James Bond 007 ? Tu parles d'un agent secret tout le monde le connaît.</q>
        </div>

    </header>

   

    <main class="container-fluid pl-0">
        <div class="container p-5" >
            <h2 id="missions">Les missions</h2>

            <!-- CARDS MISSIONS -->
            <div class="row row-cols-1 row-cols-sm-3">
                
                <?php 
                //afficher chaque entrée une à une
                while ($datas_mission = $req_mission->fetch()){
                ?>

                <div class="card m-2">
                    <div class="card-body stamp">
                        <h5 class="card-title"><?="$datas_mission[Mission_Titre] "?></h5>
                        <p class="card-text"><?="$datas_mission[Mission_Descr] "?></p>
                        <!-- <a href="#" class="btn btn-primary">Voir le détail</a> -->
                        <p class="card-text">Date de début : <?="$datas_mission[Mission_Date_Deb] "?></p>
                        <p class="card-text">Date de fin : <?="$datas_mission[Mission_Date_Fin] "?></p>
                    </div>
                </div>
  
             
                <?php
                }
                ?>
            </div>
        </div>
    </main>
    <footer>
        <nav class="nav justify-content-center">
            <a class="nav-link active" href="#missions">Les missions</a>
            <a class="nav-link" href="connexion.php">espace privé</a>
        </nav>
    </footer>

    <!--Bootstrap-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

    <!-- animation -->
    <script src="assets/JS/animation.js"></script>
</body>
</html>