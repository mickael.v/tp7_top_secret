<?php
require 'config.php';
    
try {
    // Essaye de se connecter avec PDO
    $connexion = new PDO("mysql:host=localhost;dbname=TP7;port=3306;charset=utf8", $user, $Mdp);
    echo 'connexion DB etablie';
} catch (PDOException $e) {
    // Stop le script et envoie une erreur si la connexion à échoué
    throw new PDOException($e->getMessage(), (int)$e->getCode());
}

if (isset($_POST['missionSubmit'])) {
    try {
        // je prepare ma requete
        $insert = $connexion->prepare('INSERT INTO Mission(Mission_Titre, Mission_Descr, Mission_Date_Deb,Mission_Date_Fin,Mission_Pays, Mission_Statut, Mission_Specialite, Mission_Type ) VALUES (?,?,?,?,?,?,?,?)');

        //Mes donness
        $Mission_Titre = $_POST['titre'];
        $Mission_Descr = $_POST['description'];
        $Mission_Date_Deb = $_POST['dateDeb'];
        $Mission_Date_Fin = $_POST['dateFin'];
        $Mission_Pays = $_POST['pays'];
        $Mission_Specialite = $_POST['specialite'];
        $Mission_Statut = $_POST['statut'];
        $Mission_Type = $_POST['type'];
        $Mission_Cible = $_POST['cible'];
        $Mission_Agent = $_POST['agent'];
        $Mission_Planque = $_POST['planque'];
        $Mission_Contact = $_POST['contact'];

        //je prepare les requetes pour remplir la table Mission_Cible

        $SelectMissionCible = $connexion->prepare('SELECT Cible_Code FROM Cible Where Cible_Nom = :Mission_Cible');
        $insertMissionCible = $connexion->prepare('INSERT INTO Mission_Cible (Mission_Code,Cible_Code)VALUES (?,?)');


        //j'execute ma requete qui insere la mission dans la table Mission

        $insert->execute(array($Mission_Titre, $Mission_Descr, $Mission_Date_Deb, $Mission_Date_Fin, $Mission_Pays, $Mission_Statut, $Mission_Specialite, $Mission_Type));
        //je recupere l'id de la mission que je viens d'entrer
        $Mission_Code = $connexion->lastInsertId();

        // j'execute ma requete qui recupere le code_cible et j'insere dans le tableau Mission_Cible 
        $SelectMissionCible->execute(array('Mission_Cible' => $Mission_Cible));
        $Datas = $SelectMissionCible->fetch();
        if ($Datas) {
            $insertMissionCible->execute(array($Mission_Code, $Datas['Cible_Code']));
        }

        // //je prepare les requetes pour remplir la table Mission_Agent
        $SelectMissionAgent = $connexion->prepare('SELECT Agent_Code FROM Agent Where Agent_Nom = :Mission_Agent');
        $insertMissionAgent = $connexion->prepare('INSERT INTO Mission_Agent (Mission_Code,Agent_Code)VALUES (?,?)');

        // // j'execute ma requete qui recupere l Agent_Code et j'insere dans le tableau Mission_Cible 
        $SelectMissionAgent->execute(array('Mission_Agent' => $Mission_Agent));

        $DatasAgent = $SelectMissionAgent->fetch();

        if ($DatasAgent) {
            $insertMissionAgent->execute(array($Mission_Code, $DatasAgent['Agent_Code']));
        }

        // //je prepare les requetes pour remplir la table Mission_Planque
        $SelectMissionPlanque = $connexion->prepare('SELECT Planque_Code FROM Planque Where Planque_Adresse = :Mission_Planque');
        $insertMissionPlanque = $connexion->prepare('INSERT INTO Mission_Planques (Mission_Code,Planque_Code)VALUES (?,?)');

        // // j'execute ma requete qui recupere le planque_code et j'insere dans le tableau Mission_Cible 
        $SelectMissionPlanque->execute(array('Mission_Planque' => $Mission_Planque));

        $DatasPlanque = $SelectMissionPlanque->fetch();

        if ($DatasPlanque) {
            $insertMissionPlanque->execute(array($Mission_Code, $DatasPlanque['Planque_Code']));
        }

        // //je prepare les requetes pour remplir la table Mission_Contact
        $SelectMissionContact = $connexion->prepare('SELECT Contact_Code FROM Contact Where Contact_Nom = :Mission_Contact');
        $insertMissionContact = $connexion->prepare('INSERT INTO Mission_Contact (Mission_Code,Contact_Code)VALUES (?,?)');

        // // j'execute ma requete qui recupere le Contact_code et j'insere dans le tableau Mission_Cible 
        $SelectMissionContact->execute(array('Mission_Contact' => $Mission_Contact));

        $DatasContact = $SelectMissionContact->fetch();

        if ($DatasPlanque) {
            $insertMissionContact->execute(array($Mission_Code, $DatasContact['Contact_Code']));
        }

        // //je prepare les requetes pour remplir la table Mission_Statut
        $SelectMissionStatut = $connexion->prepare('SELECT Code_Statut FROM Statut Where Code_Statut = :Mission_Statut');
        $insertMissionStatut = $connexion->prepare('INSERT INTO Mission_Statut (Mission_Code,Code_Statut)VALUES (?,?)');

        // // j'execute ma requete qui recupere le Code_Statut et j'insere dans le tableau Mission_Cible 
        $SelectMissionStatut->execute(array('Mission_Statut' => $Mission_Statut));
        var_dump($Mission_Statut);
        $DatasStatut = $SelectMissionStatut->fetch();
        var_dump($DatasStatut);
        if ($DatasStatut) {
            $insertMissionStatut->execute(array($Mission_Code, $DatasStatut['Code_Statut']));
        }

        //succés

        echo "insertion OK";
    } catch (PDOException $e) {
        die("pas inséré : " . $e->getMessage());
    }
}
header("Location: $url");
?>
