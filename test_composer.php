<?php
require 'vendor/autoload.php';

use Money\Money;

$value1 = Money::EUR(800);       // €8.00
$value2 = Money::EUR(500);       // €5.00

$result = $value1->add($value2); // €13.00

var_dump ($result);


?>