<!-- PHP -->
<?php 
    require 'config.php';

    try {
        // Essaye de se connecter avec PDO
        $connexion = new PDO("mysql:host=localhost;dbname=TP7;port=3306;charset=utf8", $user, $Mdp);
        
        echo 'connexion DB etablie';
    } catch (PDOException $e) {
        // Stop le script et envoie une erreur si la connexion à échoué
        throw new PDOException($e->getMessage(), (int)$e->getCode());
    }
    
    $msg = 'connexion à votre compte utilisateur';

    //Obtenir la saisie de l'internaute
    $mail = $_POST['mail'];
    $mdp = $_POST['mp'];

   

    //vérifier que l'email existe
    $isEmailExist = $connexion->prepare('SELECT * FROM Administation WHERE Admin_Mail=:mail');
    $isEmailExist->execute(['mail' => $mail]);
    $data = $isEmailExist->fetch();
    
 
    if(!$data && !password_verify($mdp, $data['Admin_Mdp'])){
        
        header('Location: connexion.php');
    
    } else{
        
        session_start();

        $_SESSION['username'] = $data['Admin_Prenom'];
        $_SESSION['email'] = $data['Admin_Mail'];

        header('Location: accueilAdmin.php');
    }
    

    
?>