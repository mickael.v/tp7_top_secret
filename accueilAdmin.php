<?php
session_start();
    require 'config.php';
    require 'jointure.php';
    
    //OUVRIR LA SESSION
    

    if(isset($_SESSION['email'])) {
        
    }else {

        header('Location: connexion.php');
    }
    
    //CONNECTION A LA BDD
    try {
        // Essaye de se connecter avec PDO
        $connexion = new PDO("mysql:host=localhost;dbname=TP7;port=3306;charset=utf8", $user, $Mdp);
        
       
    } catch (PDOException $e) {
        // Stop le script et envoie une erreur si la connexion à échoué
        throw new PDOException($e->getMessage(), (int)$e->getCode());
    }

    //récupère tout le contenu de la table mission
    $req_mission=$connexion->query('SELECT * FROM Mission');

    //récupère tout le contenu de la table agent
    $req_agent=$connexion->query('SELECT * FROM Agent');

    //récupère tout le contenu de la table cible
    $req_cible=$connexion->query('SELECT * FROM Cible');

    //récupère tout le contenu de la table contact
    $req_contact=$connexion->query('SELECT * FROM Contact');

    //récupère tout le contenu de la table cible
    $req_planque=$connexion->query('SELECT * FROM Planque');

    //récupère tout le contenu de la table Pays
    $req_pays=$connexion->query('SELECT * FROM Pays');

    //récupère tout le contenu de la table Statut
    $req_statut=$connexion->query('SELECT * FROM Statut');
    
    //récupère tout le contenu de la table Type-Planque
    $req_typePlanque=$connexion->query('SELECT * FROM Type_Planque');

    //récupère tout le contenu de la table Specialite
    $req_specialite=$connexion->query('SELECT * FROM Specialite');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>M&M Agency - Accueil Administrateur</title>

    <!--Bootstrap-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>

    <!--Mon CSS-->
    <link rel="stylesheet" href="./dist/prod.css">
</head>

<body>
    <header class="container-fluid ">

        <nav class=" d-flex bd-highlight mb-3 navbar navbar-expand-lg navbar-dark">
            <a class="flex-md-grow-1 navbar-brand mr-auto p-2 bd-highlight" href="#">
                <svg width="60" height="60" class="d-inline-block align-top" alt="Logo M&M Agency" loading="lazy"  viewBox="-44 0 512 512.00002" xmlns="http://www.w3.org/2000/svg">
                    <style type="text/css">
                        .st1{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
                        .st2{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
                        .st3{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
                        .st4{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
                        .st5{fill-rule:evenodd;clip-rule:evenodd;fill:#FFFFFF;}
                    </style>
                    <path class="st1" d="m400.675781 219.242188h-11.058593l-30.875-146.214844c-4.34375-20.585938-15.738282-39.222656-32.082032-52.476563-16.34375-13.253906-36.933594-20.550781-57.972656-20.550781-3.03125 0-5.898438 1.355469-7.820312 3.699219-12.125 14.765625-30.035157 23.378906-49.140626 23.628906-.28125.003906-.566406.003906-.847656.003906-18.792968 0-36.570312-8.113281-48.886718-22.34375l-1.289063-1.492187c-1.925781-2.21875-4.71875-3.496094-7.652344-3.496094-44.101562 0-82.121093 31.4375-90.40625 74.753906l-27.632812 144.488282h-10.894531c-13.300782 0-24.117188 10.816406-24.117188 24.113281v27.035156c0 13.300781 10.816406 24.117187 24.117188 24.117187h12.832031c.449219 36.921876 30.609375 66.820313 67.632812 66.820313h9.886719c37.027344 0 67.183594-29.898437 67.632812-66.820313h62.863282c.449218 36.921876 30.609375 66.820313 67.632812 66.820313h9.886719c37.023437 0 67.183594-29.898437 67.632813-66.820313h10.558593c13.296875 0 24.117188-10.816406 24.117188-24.117187v-27.035156c0-13.296875-10.816407-24.113281-24.117188-24.113281zm-341.765625-17.207032h228.609375c5.585938 0 10.117188-4.53125 10.117188-10.117187 0-5.589844-4.53125-10.121094-10.117188-10.121094h-224.742187l19.746094-103.242187c6.179687-32.324219 33.597656-56.175782 66.085937-58.179688 16.320313 17.625 39.269531 27.523438 63.382813 27.1875 23.378906-.304688 45.394531-10.148438 61.167968-27.183594 31.851563 2.003906 59.125 25.285156 65.785156 56.828125l29.988282 142.035157h-313.316406zm55.558594 139.054688h-9.886719c-25.867187 0-46.960937-20.820313-47.410156-46.582032h104.710937c-.449218 25.761719-21.546874 46.582032-47.414062 46.582032zm208.015625 0h-9.886719c-25.867187 0-46.960937-20.820313-47.410156-46.582032h104.710938c-.449219 25.761719-21.546876 46.582032-47.414063 46.582032zm82.074219-70.699219c0 2.140625-1.742188 3.878906-3.878906 3.878906h-376.5625c-2.140626 0-3.878907-1.738281-3.878907-3.878906v-27.035156c0-2.136719 1.742188-3.878907 3.878907-3.878907h376.5625c2.136718 0 3.878906 1.742188 3.878906 3.878907zm0 0"/><path class="st2" d="m221.6875 350.417969c2.214844-5.128907-.144531-11.085938-5.277344-13.300781-5.128906-2.21875-11.085937.144531-13.300781 5.273437l-31.023437 71.792969c-3.4375 7.945312-2.648438 17.011718 2.105468 24.246094 4.753906 7.234374 12.765625 11.554687 21.421875 11.554687h2.355469c5.589844 0 10.117188-4.53125 10.117188-10.117187 0-5.589844-4.527344-10.117188-10.117188-10.117188h-2.355469c-2.628906 0-4.027343-1.703125-4.511719-2.433594-.480468-.730468-1.484374-2.691406-.441406-5.105468zm0 0"/><path class="st3" d="m242.53125 491.761719h-60.269531c-5.589844 0-10.117188 4.53125-10.117188 10.121093 0 5.585938 4.527344 10.117188 10.117188 10.117188h60.269531c5.589844 0 10.117188-4.53125 10.117188-10.117188 0-5.589843-4.527344-10.121093-10.117188-10.121093zm0 0"/><path class="st4" d="m336.605469 187.9375c-2.09375-5.179688-7.992188-7.683594-13.175781-5.585938-5.179688 2.09375-7.679688 7.992188-5.582032 13.175782l.148438.367187c1.589844 3.9375 5.378906 6.328125 9.382812 6.328125 1.261719 0 2.546875-.238281 3.789063-.742187 5.179687-2.097657 7.683593-7.996094 5.585937-13.175781zm0 0"/><path class="st5" d="m237.847656 450.09375c1.265625 0 2.546875-.234375 3.792969-.738281 5.179687-2.097657 7.679687-7.996094 5.585937-13.175781l-.148437-.367188c-2.097656-5.179688-7.996094-7.683594-13.175781-5.585938-5.183594 2.09375-7.683594 7.992188-5.585938 13.175782l.148438.367187c1.59375 3.9375 5.382812 6.324219 9.382812 6.324219zm0 0"/>
                </svg>
                <h1 class="d-inline-block align-top">M&M Agency</h1>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse p-2 bd-highlight justify-content-end" id="navbarSupportedContent">
                <ul class="navbar-nav">
                    <li class="nav-item active mr-2">
                        <a class="nav-link" href="#missions">
                            <?="bonjour $_SESSION[username]"?>
                        </a>
                    </li>
                    <li class="nav-item mr-2">
                        <button type="button" class="btn btn-light"><a href="exe_deconnexion.php">Me déconnecter</a></button>
                    </li>
                </ul>
            </div>
        </nav>

    </header>



    <main class="container-fluid p-5">

        <h3 class="animate__animated animate__animate__fadeInLeftBig"><?="Bonjour grand maître $_SESSION[username]! </br> 
        Bienvenue sur la base secrète. Vous avez la main sur l'ensemble des missions."?></h3>
       
        <!-- LES MISSIONS -->
        <div class="container p-5">
            <div class="row border-bottom border-secondary justify-content-between">
                <h2 id="missions">Les missions</h2>
                <button type="button" class="btn border-primary rounded" data-toggle="collapse" href="#collapseMissions" role="button" aria-expanded="false" aria-controls="collapseExample">
                    <span class="float-right">
                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="12">
                            <path fill="none" stroke="#5267DF" stroke-width="3" d="M1 1l8 8 8-8"/>
                        </svg>
                    </span>
                </button>
            </div>

            <!-- CARDS MISSIONS -->
            <div class="collapse" id="collapseMissions">
                <div class="row row-cols-1 row-cols-sm-3">

                    <?php 
                    //afficher chaque entrée une à une
                    while ($datas_mission = $req_mission->fetch()){
                    ?>
                    <div class="card border-secondary m-3">
                        <div class="card-header bg-transparent border-secondary">
                            <h5 class='card-title'><?="Mission: $datas_mission[Mission_Titre]"?></h5>
                        </div>
                        <div class="card-body stamp pl-2">
                            <div class="row align-items-baseline pl-3">
                                <h6>Description:</h6>
                                <p class="ml-3"><?="$datas_mission[Mission_Descr]"?></p>
                            </div>
                            <div class="row align-items-baseline pl-3">
                                <h6>Lieu:</h6>
                                <p class="ml-3"><?="$datas_mission[Mission_Pays]"?></p>
                            </div>
                            <div class="row align-items-baseline pl-3">
                                <h6>Dates:</h6>
                                <p class="ml-3"><?="du $datas_mission[Mission_Date_Deb] au $datas_mission[Mission_Date_Fin]"?></p>
                            </div>
                            <div class="row align-items-baseline pl-3">
                                <h6>Type de mission:</h6>
                                <p class="ml-3"><?="$datas_mission[Mission_Type]"?></p>
                            </div>
                            
                            <div class="row align-items-baseline pl-3">
                                <h6>Agent:</h6>
                                <p class="ml-3"><?= $_SESSION['agentnom']?></p>
                            </div>
                            <div class="row align-items-baseline pl-3">
                                <h6>Cible:</h6>
                               
                                <p class="ml-3"><?= $_SESSION["cible"] ?></p>
                            </div>
                            <div class="row align-items-baseline pl-3">
                                <h6>Contact:</h6>
                                <p class="ml-3"><?=$_SESSION['contact']?></p>
                            </div>
                            <div class="row align-items-baseline pl-3">
                                <h6>Statut:</h6>
                                <p class="ml-3"><?=$_SESSION['statut']?></p>
                            </div>
                            <div class="row align-items-baseline pl-5">
                                <h6>Planque:</h6>
                                <p class="ml-3"><?=$_SESSION['planque']?></p>
                            </div>

                        </div>
                        <div class="card-footer bg-transparent border-secondary justify-content-end">
                            <button type="button" class="btn btn-primary">Modifier</button>
                            <button type="button" class="btn btn-danger">
                                <a id="Mission_<?= "$datas_mission[Mission_Code]"?>" href="deleteMission.php?code=<?= "$datas_mission[Mission_Code]"?>">Supprimer</a>
                            </button>
                        </div>
                    </div>
                    <?php
                    }
                    ?>
                </div>

                <!-- BOUTTON AJOUTER -->
                <button type="button" class="btn btn-outline-primary mt-5" data-toggle="modal" data-target="#ajoutMission">
                    + Ajouter une mission
                </button>
            </div>

            <!-- MODAL : FORMULAIRE MISSION -->
            <div class="modal fade" id="ajoutMission" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">>
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="AjoutMission">Nouvelle mission</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <!--FORMULAIRE AJOUT MISSION-->
                            <form method="POST" action="Mission.php">
                                <div class="form-group">
                                    <!--Titre mission-->
                                    <label for="titre">Titre de la mission:</label>
                                    <input type="text" class="form-control" id="missionTitre" placeholder="entrer le titre de la mission ici" name="titre">

                                    <!-- Description -->
                                    <label for="description">Description:</label>
                                    <input type="text" class="form-control" id="missionDescription" placeholder="entrer une description ici" name="description">
                                     
                                    <!-- Date Début -->
                                    <label for="dateDébut">Date de début:</label>
                                    <input type="text" class="form-control" id="dateDeb"  placeholder="AAAA-MM-JJ" name="dateDeb">

                                    <!-- Date Fin -->
                                    <label for="dateFin">Date de fin:</label>
                                    <input type="text" class="form-control" id="dateFin"  placeholder="AAAA-MM-JJ" name="dateFin">   
                                </div>

                                 <!-- type de mission -->
                                    <label for="type de mission">Type de mission</label>
                                    <input type="text" class="form-control" id="missionType placeholder="entrer un typede mission ici" name="type">
                                </div>

                                <!-- Pays de la mission -->
                                <div class="form-group">
                                    <label for="pays de la mission">Pays de la Mission :</label>
                                    <select class="form-control" id="missionPays" name="pays">
                                        <option value="" disabled>Choisir un pays</option>
                                        <option value="1" selected>France</option>
                                        <option value="2">Russie</option>
                                        <option value="3">Chine</option>
                                        <option value="4">Royaume-Uni</option>
                                        <option value="5">USA</option>
                                        <option value="6">Allemagne</option>
                                        <option value="7">Corée du Nord</option>
                                        <option value="8">Turquie</option>
                                        <option value="9">Afrique du Sud</option>
                                    </select>
                                </div>

                                <!-- Specialité requise-->
                                <div class="form-group">
                                    <label for="Spécialité requise">Selectionnez une specialité requise :</label>
                                    <select class="form-control" id="missionSpécialité" name="specialite">
                                        <option value="" disabled>Choisir une specialité</option>
                                        <option value="1" selected>Enlevement</option>
                                        <option value="2">Infiltration</option>
                                        <option value="3">Espionnage</option>
                                        <option value="4">Sabotage</option>
                                        <option value="5">Corps à corps</option>
                                        <option value="6">Sniper</option>
                                        <option value="7">Explosif</option>
                                        <option value="8">Empoisonnement</option>
                                        <option value="9">Piratage informatique</option>
                                    </select>
                                </div>

                                <!-- statut -->
                                <div class="form-group">
                                    <label for="statut">Statut</label>
                                    <select class="form-control" id="missionStatut" name="statut">
                                        <option value="" disabled>Choisir un Statut</option>
                                        <option value="1" selected>En attente</option>
                                        <option value="2">En cours</option>
                                        <option value="3">Echec</option>
                                        <option value="4">Abandon</option>
                                        <option value="5">Succès</option>
                                    </select>
                                </div>


                                <!-- choix de l'agent -->
                                <div class="form-group">
                                <?php
                                $stmt = $connexion->prepare('SELECT Agent_Nom FROM Agent');
                                $stmt->execute();
                                $datas = $stmt->fetchAll();
                                ?>
                                    <label for="agent">Choisissez un agent pour cette mission</label>
                                    <select class="form-control" id="missionAgent" name="agent">
                                    <?php foreach ($datas as $Agent_Nom) { ?>
                                        <option value="<?= $Agent_Nom['Agent_Nom'] ?>">
                                            <?= $Agent_Nom['Agent_Nom'] ?>
                                        </option>
                                    <?php } ?>
                                    </select>
                                </div>

                                <!-- choix de la cible -->
                                <div class="form-group">
                                <?php
                                $stmt = $connexion->prepare('SELECT Cible_Nom FROM Cible');
                                $stmt->execute();

                                $datas = $stmt->fetchAll();
                                ?>
                                    <label for="cible">Designez une cible</label>
                                    <select class="form-control" id="missionAgent" name="cible">
                                    <?php foreach ($datas as $Cible_Nom) { ?>
                                        <option value="<?= $Cible_Nom['Cible_Nom'] ?>">
                                            <?= $Cible_Nom['Cible_Nom'] ?>
                                        </option>
                                    <?php } ?>
                                    </select>
                                </div>

                                <!-- choix de la planque -->
                                <div class="form-group">
                                <?php
                                $stmt = $connexion->prepare('SELECT Planque_Adresse FROM Planque');
                                $stmt->execute();
                                $datas = $stmt->fetchAll();
                                ?>
                                    <label for="planque"> Choisissez une planque pour cette mission</label>
                                    <select class="form-control" name="planque">
                                        <?php foreach ($datas as $Planque_Adresse) { ?>
                                        <option value="<?= $Planque_Adresse['Planque_Adresse']?>">
                                            <?= $Planque_Adresse['Planque_Adresse'] ?>
                                        </option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <!-- choix du contact -->
                                <div class="form-group">
                                <?php
                                $stmt = $connexion->prepare('SELECT Contact_Nom FROM Contact');
                                $stmt->execute();
                                $datas = $stmt->fetchAll();
                                ?>
                                    <label for="contact"> Choisissez un contact pour cette mission </label>
                                    <select class="form-control" id="contact" name="contact">
                                    <?php foreach ($datas as $Contact_Nom) { ?>
                                        <option value="<?= $Contact_Nom['Contact_Nom'] ?>">
                                            <?= $Contact_Nom['Contact_Nom'] ?>
                                        </option>
                                    <?php } ?>
                                    </select>
                                </div>

                                <input type="submit" class="btn btn-primary" value="Envoyez" name="missionSubmit">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- LES AGENTS -->
        <div class="container p-5">
            <div class="row border-bottom border-secondary justify-content-between">
                <h2 id="agents ">Les agents</h2>
                <button type="button" class="btn border-primary rounded" data-toggle="collapse" href="#collapseAgents" role="button" aria-expanded="false" aria-controls="collapseExample">
                    <span class="float-right">
                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="12">
                            <path fill="none" stroke="#5267DF" stroke-width="3" d="M1 1l8 8 8-8"/>
                        </svg>
                    </span>
                </button>
            </div>

    
            <!-- CARDS AGENTS -->
            <div class="collapse" id="collapseAgents">
                <div class="row row-cols-1 row-cols-sm-3">

                    <?php 
                    //afficher chaque entrée une à une
                    while ($datas_agent = $req_agent->fetch()){
                    ?>
                    <div class="card border-secondary m-4">
                        <div class="card-header bg-transparent border-secondary">
                            <h5 class='card-title'><?="$datas_agent[Agent_Prenom] $datas_agent[Agent_Nom]"?></h5>
                        </div>
                        <div class="card-body">
                            <div class="row align-items-baseline pl-3">
                                <h6>Nom:</h6>
                                <p class="ml-3"><?="$datas_agent[Agent_Nom]"?></p>
                            </div>
                            <div class="row align-items-baseline pl-3">
                                <h6>Prénom:</h6>
                                <p class="ml-3"><?="$datas_agent[Agent_Prenom]"?></p>
                            </div>
                            <div class="row align-items-baseline pl-3">
                                <h6>Code:</h6>
                                <p class="ml-3"><?="$datas_agent[Agent_Code]"?></p>
                            </div>
                            <div class="row align-items-baseline pl-3">
                                <h6>Date de naissance:</h6>
                                <p class="ml-3"><?="$datas_agent[Agent_Date_Naiss]"?></p>
                            </div>
                            <div class="row align-items-baseline pl-3">
                                <h6>Nationalité:</h6>
                                <p class="ml-3"><?="$datas_agent[Agent_Nationalité]"?></p>
                            </div>
                            <div class="row align-items-baseline pl-3">
                                <h6>Spécialité:</h6>
                                <p class="ml-3"><?="$datas_agent[Agent_Code]"?></p>
                            </div>
                        </div>
                        <div class="card-footer bg-transparent border-secondary justify-content-end">
                            <button type="button" class="btn btn-primary">Modifier</button>
                            <button type="button" class="btn btn-danger">
                                <a id="Agent_<?= "$datas_agent[Agent_Code]"?>" href="deleteAgent.php?code=<?= "$datas_agent[Agent_Code]"?>">Supprimer</a>
                            </button>
                        </div>
                    </div>
                    <?php
                    }
                    ?>
                </div>

                <!-- BOUTTON AJOUTER -->
                <button type="button" class="btn btn-outline-primary mt-5" data-toggle="modal" data-target="#ajoutAgent">
                    + Ajouter un Agent
                </button>
               
            </div>

            <!-- MODAL : FORMULAIRE AGENT -->
            <div class="modal fade" id="ajoutAgent" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Nouvel Agent</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <!--FORMULAIRE AJOUT AGENT-->
                            <form method="POST" action="agent.php">
                                <div class="form-group">
                                    <!--name-->
                                    <label for="Nom">Nom:</label>
                                    <input type="text" class="form-control" id="Nom" placeholder="entrer le Nom de l'agent ici" name="name">

                                    <!-- FirstName -->
                                    <label for="Nom">Prénom:</label>
                                    <input type="text" class="form-control" id="prénom" placeholder="entrer le Prénom de l'agent ici" name="firstname">

                                    <!-- Birth -->
                                    <label for="date">Date de naissance:</label>
                                    <input type="text" class="form-control"id="date"  placeholder="AAAA-MM-JJ" name="birth">
                                </div>

                                    <!-- Speciality -->
                                <div class="form-group">
                                    <label for="SpécialitéAgent">Spécialité</label>
                                    <select class="form-control" id="SpécialitéAgent" name="specialite">
                                        <option value="" disabled>Choisir une specialité</option>
                                        <option value="1" selected>Enlevement</option>
                                        <option value="2">Infiltration</option>
                                        <option value="3">Espionnage</option>
                                        <option value="4">Sabotage</option>
                                        <option value="5">Corps à corps</option>
                                        <option value="6">Sniper</option>
                                        <option value="7">Explosif</option>
                                        <option value="8">Empoisonnement</option>
                                        <option value="9">Piratage informatique</option>
                                    </select>
                                </div>

                                    <!-- Nationalité -->
                                <div class="form-group">
                                    <label for="nationalitéAgent">Nationalité</label>
                                    <select class="form-control" id="nationalitéAgent" name="nationalite">
                                        <option value="" disabled>Choisir une nationalité</option>
                                        <option value="1" selected>France</option>
                                        <option value="2">Russie</option>
                                        <option value="3">Chine</option>
                                        <option value="4">Royaume-Uni</option>
                                        <option value="5">USA</option>
                                        <option value="6">Allemagne</option>
                                        <option value="7">Corée du Nord</option>
                                        <option value="8">Turquie</option>
                                        <option value="9">Afrique du Sud</option>
                                    </select>
                                </div>
                                <input type="submit" class="btn btn-primary" value="Envoyez" name="agentSubmit" onclick="">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- LES CIBLES -->
        <div class="container p-5">
            <div class="row border-bottom border-secondary justify-content-between">
                <h2 id="cibles ">Les cibles</h2>
                <button type="button" class="btn border-primary rounded" data-toggle="collapse" href="#collapseCibles" role="button" aria-expanded="false" aria-controls="collapseExample">
                    <span class="float-right">
                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="12">
                            <path fill="none" stroke="#5267DF" stroke-width="3" d="M1 1l8 8 8-8"/>
                        </svg>
                    </span>
                </button>
            </div>
        
            <!-- CARDS CIBLES -->
            <div class="collapse" id="collapseCibles">
                <div class="row row-cols-1 row-cols-sm-3">

                    <?php 
                    //afficher chaque entrée une à une
                    while ($datas_cible = $req_cible->fetch()) {
                        // var_dump($datas_cible);
                    ?>
                    <div class="card border-secondary m-4">
                        <div class="card-header bg-transparent border-secondary">
                            <h5 class="card-title"><?="$datas_cible[Cible_Prenom] $datas_cible[Cible_Nom]"?></h5>
                        </div>
                        <div class="card-body">
                            <div class="row align-items-baseline pl-3">
                                <h6>Nom:</h6>
                                <p class="ml-3"><?="$datas_cible[Cible_Nom]"?></p>
                            </div>
                            <div class="row align-items-baseline pl-3">
                                <h6>Prénom:</h6>
                                <p class="ml-3"><?="$datas_cible[Cible_Prenom]"?></p>
                            </div>
                            <div class="row align-items-baseline pl-3">
                                <h6>Code:</h6>
                                <p class="ml-3"><?="$datas_cible[Cible_Code]"?></p>
                            </div>
                            <div class="row align-items-baseline pl-3">
                                <h6>Date de naissance:</h6>
                                <p class="ml-3"><?="$datas_cible[Cible_Date_Naiss]"?></p>
                            </div>
                            <div class="row align-items-baseline pl-3">
                                <h6>Nationalité:</h6>
                                <p class="ml-3"><?="$datas_cible[Cible_Nationalité]"?></p>
                            </div>
                        </div>
                        <div class="card-footer bg-transparent border-secondary justify-content-end">
                            <button type="button" class="btn btn-primary">Modifier</button>
                            <button type="button" class="btn btn-danger">
                                <a id="Cible_<?= "$datas_cible[Cible_Code]"?>" href="deleteCible.php?code=<?= "$datas_cible[Cible_Code]"?>">Supprimer</a>
                            </button>
                        </div>
                    </div>
                    <?php
                    }
                    ?>
                </div>

                <!-- BOUTTON AJOUTER -->
                <button type="button" class="btn btn-outline-primary mt-5" data-toggle="modal" data-target="#ajoutCible">
                    + Ajouter une cible
                </button>
            </div>

            <!-- MODAL : FORMULAIRE AJOUT CIBLE -->
            <div class="modal fade" id="ajoutCible" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Nouvelle cible</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <!--FORMULAIRE AJOUT CIBLE-->
                            <form method="POST" action="cible.php">
                                <div class="form-group">
                                    <!--name-->
                                    <label for="Nom">Nom:</label>
                                    <input type="text" class="form-control" id="Nom" placeholder="entrer le Nom de la cible ici" name="name">

                                    <!-- FirstName -->
                                    <label for="Nom">Prénom:</label>
                                    <input type="text" class="form-control" id="prénom" placeholder="entrer le Prénom de la cible ici" name="firstname">

                                    <!-- Birth -->
                                    <label for="date">Date de naissance:</label>
                                    <input type="text" class="form-control"id="date"  placeholder="AAAA-MM-JJ" name="birth">
                                </div>

                                    <!-- Nationalité -->
                                <div class="form-group">
                                    <label for="nationalitéCible">Nationalité</label>
                                    <select class="form-control" id="nationalitéCible" name="nationalite">
                                        <option value="" disabled>Choisir une nationalité</option>
                                        <option value="1" selected>France</option>
                                        <option value="2">Russie</option>
                                        <option value="3">Chine</option>
                                        <option value="4">Royaume-Uni</option>
                                        <option value="5">USA</option>
                                        <option value="6">Allemagne</option>
                                        <option value="7">Corée du Nord</option>
                                        <option value="8">Turquie</option>
                                        <option value="9">Afrique du Sud</option>
                                    </select>
                                </div>
                                <input type="submit" class="btn btn-primary" value="Envoyez" name="cibleSubmit" onclick="location='/accueilAdmin'">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- LES CONTACTS -->
        <div class="container p-5">
            <div class="row border-bottom border-secondary justify-content-between">
                <h2 id="contacts">Les contacts</h2>
                <button type="button" class="btn border-primary rounded" data-toggle="collapse" href="#collapseContacts" role="button" aria-expanded="false" aria-controls="collapseExample">
                    <span class="float-right">
                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="12">
                            <path fill="none" stroke="#5267DF" stroke-width="3" d="M1 1l8 8 8-8"/>
                        </svg>
                    </span>
                </button>
            </div>
        
            <!-- CARDS CONTACTS -->
            <div class="collapse" id="collapseContacts">
                <div class="row row-cols-1 row-cols-sm-3">

                    <?php 
                    //afficher chaque entrée une à une
                    while ($datas_contact = $req_contact->fetch()){
                    ?>
                    <div class="card border-secondary mt-4" min-width="">
                        <div class="card-header bg-transparent border-secondary">
                            <h5 class='card-title'><?="$datas_contact[Contact_Prenom] $datas_contact[Contact_Nom]"?></h5>
                        </div>
                        <div class="card-body">
                            <div class="row align-items-baseline pl-3">
                                <h6>Nom:</h6>
                                <p class="ml-3"><?="$datas_contact[Contact_Nom]"?></p>
                            </div>
                            <div class="row align-items-baseline pl-3">
                                <h6>Prénom:</h6>
                                <p class="ml-3"><?="$datas_contact[Contact_Prenom]"?></p>
                            </div>
                            <div class="row align-items-baseline pl-3">
                                <h6>Code:</h6>
                                <p class="ml-3"><?="$datas_contact[Contact_Code]"?></p>
                            </div>
                            <div class="row align-items-baseline pl-3">
                                <h6>Date de naissance:</h6>
                                <p class="ml-3"><?="$datas_contact[Contact_Date_Naiss]"?></p>
                            </div>
                            <div class="row align-items-baseline pl-3">
                                <h6>Nationalité:</h6>
                                <p class="ml-3"><?="$datas_contact[Contact_Nationalité]"?></p>
                            </div>
                        </div>
                        <div class="card-footer bg-transparent border-secondary justify-content-end">
                            <button type="button" class="btn btn-primary">Modifier</button>
                            <button type="button" class="btn btn-danger">
                                <a id="Contact_<?= "$datas_contact[Contact_Code]"?>" href="deleteContact.php?code=<?= "$datas_contact[Contact_Code]"?>">Supprimer</a>
                            </button>
                        </div>
                    </div>
                    <?php
                    }
                    // Termine le traitement de la requête
                    $req_cible->closeCursor(); 
                    ?>
                </div>

                <!-- BOUTTON AJOUTER -->
                <button type="button" class="btn btn-outline-primary mt-5" data-toggle="modal" data-target="#ajoutContact">
                    + Ajouter un contact
                </button>
            </div>
                

            <!-- MODAL : FORMULAIRE CONTACT -->
            <div class="modal fade" id="ajoutContact" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Nouveau contact</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <!--FORMULAIRE AJOUT CONTACT-->
                            <form method="POST" action="contact.php">
                                <div class="form-group">
                                    <!--name-->
                                    <label for="Nom">Nom:</label>
                                    <input type="text" class="form-control" id="Nom" placeholder="entrer le Nom du contact ici" name="name">

                                    <!-- FirstName -->
                                    <label for="Nom">Prénom:</label>
                                    <input type="text" class="form-control" id="prénom" placeholder="entrer le Prénom du contact ici" name="firstname">

                                    <!-- Birth -->
                                    <label for="date">Date de naissance:</label>
                                    <input type="text" class="form-control"id="date"  placeholder="AAAA-MM-JJ" name="birth">
                                </div>

                                    <!-- Nationalité -->
                                <div class="form-group">
                                    <label for="nationalitéContact">Nationalité</label>
                                    <select class="form-control" id="nationalitéContact" name="nationalite">
                                        <option value="" disabled>Choisir une nationalité</option>
                                        <option value="1" selected>France</option>
                                        <option value="2">Russie</option>
                                        <option value="3">Chine</option>
                                        <option value="4">Royaume-Uni</option>
                                        <option value="5">USA</option>
                                        <option value="6">Allemagne</option>
                                        <option value="7">Corée du Nord</option>
                                        <option value="8">Turquie</option>
                                        <option value="9">Afrique du Sud</option>
                                    </select>
                                </div>
                                <input type="submit" class="btn btn-primary" value="Envoyez" name="contactSubmit">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- LES PLANQUES -->
        <div class="container p-5">
            <div class="row border-bottom border-secondary justify-content-between">
                <h2 id="planques">Les planques</h2>
                <button type="button" class="btn border-primary rounded" data-toggle="collapse" href="#collapsePlanques" role="button" aria-expanded="false" aria-controls="collapseExample">
                    <span class="float-right">
                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="12">
                            <path fill="none" stroke="#5267DF" stroke-width="3" d="M1 1l8 8 8-8"/>
                        </svg>
                    </span>
                </button>
            </div>
        
            <!-- CARDS PLANQUES -->
            <div class="collapse" id="collapsePlanques">
                <div class="row row-cols-1 row-cols-sm-3">

                    <?php 
                    //afficher chaque entrée une à une
                    while ($datas_planque = $req_planque->fetch()){
                    ?>
                    <div class="card border-secondary m-4" min-width="">
                        <div class="card-header bg-transparent border-secondary">
                            <h5 class='card-title'><?="Planque n° $datas_planque[Planque_Code]"?></h5>
                        </div>
                        <div class="card-body">
                            <div class="row align-items-baseline pl-3">
                                <h6>Adresse:</h6>
                                <p class="ml-3"><?="$datas_planque[Planque_Adresse]"?></p>
                            </div>
                            <div class="row align-items-baseline pl-3">
                                <h6>Pays:</h6>
                                <p class="ml-3"><?="$datas_planque[Planque_Pays]"?></p>
                            </div>
                            <div class="row align-items-baseline pl-3">
                                <h6>Code:</h6>
                                <p class="ml-3"><?="$datas_planque[Planque_Code]"?></p>
                            </div>
                            <div class="row align-items-baseline pl-3">
                                <h6>Type:</h6>
                                <p class="ml-3"><?="$datas_planque[Planque_Type]"?></p>
                            </div>
                        </div>
                        <div class="card-footer bg-transparent border-secondary justify-content-end">
                            <button type="button" class="btn btn-primary">
                                <a>Modifier</a>
                            </button>
                            <button type="submit" class="btn btn-danger">
                                <a id="Planque_<?= "$datas_planque[Planque_Code]"?>" href="deletePlanque.php?code=<?= "$datas_planque[Planque_Code]"?>">Supprimer</a>
                            </button>
                        </div>
                    </div>
                    <?php
                    }
                    // Termine le traitement de la requête
                    //$req_cible->closeCursor(); 
                    ?>
                </div>

                <!-- BOUTTON AJOUTER -->
                <button type="button" class="btn btn-outline-primary mt-5" data-toggle="modal" data-target="#ajoutPlanque">
                    + Ajouter une planque
                </button>
            </div>


            <!-- MODAL : FORMULAIRE PLANQUE -->
            <div class="modal fade" id="ajoutPlanque" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Nouvelle planque</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <!--FORMULAIRE AJOUT PLANQUE-->
                            <form method="POST" action="planque.php">
                                <div class="form-group">
                                    <!--adresse-->
                                    <label for="Nom">Adresse:</label>
                                    <input type="text" class="form-control" id="Adresse" placeholder="entrer l'adresse ici" name="adresse">

                                    <!-- Pays -->
                                <div class="form-group">
                                    <label for="paysPlanque">Pays de la planque</label>
                                    <select class="form-control" id="paysPlanque" name="pays">
                                        <option value="" disabled>Choisir un Pays</option>
                                        <option value="1" selected>France</option>
                                        <option value="2">Russie</option>
                                        <option value="3">Chine</option>
                                        <option value="4">Royaume-Uni</option>
                                        <option value="5">USA</option>
                                        <option value="6">Allemagne</option>
                                        <option value="7">Corée du Nord</option>
                                        <option value="8">Turquie</option>
                                        <option value="9">Afrique du Sud</option>
                                    </select>
                                </div>

                                    <!-- Type de planque -->
                                <div class="form-group">
                                    <label for="typePlanque">Pays de la planque</label>
                                    <select class="form-control" id="typePlanque" name="type">
                                        <option value="" disabled>Choisir un type de planque</option>
                                        <option value="1" selected>Maison</option>
                                        <option value="2">Appartement</option>
                                        <option value="3">Studio</option>
                                        <option value="4">Hotel</option>
                                        <option value="5">Bateau</option>
                                        <option value="6">Cabane</option>
                                        <option value="7">Usine desafectée</option>
                                        <option value="8">Ferme</option>
                                    </select>
                                </div>
                                <input type="submit" class="btn btn-primary" value="Envoyez" name="planqueSubmit">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


         <!-- LE Referentiel des codes -->
         <div class="container p-5">
            <div class="row border-bottom border-secondary justify-content-between">
                <h2 id="planques">Le Referentiel des codes</h2>
                <button type="button" class="btn border-primary rounded" data-toggle="collapse" href="#collapseReferentiel" role="button" aria-expanded="false" aria-controls="collapseExample">
                    <span class="float-right">
                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="12">
                            <path fill="none" stroke="#5267DF" stroke-width="3" d="M1 1l8 8 8-8"/>
                        </svg>
                    </span>
            </div>
            <!-- CARDS Referentiel Pays -->
            <div class="collapse" id="collapseReferentiel">
                <div class="row row-cols-3">
                    <div class="card border-secondary m-4" min-width="">
                        <div class="card-header bg-transparent border-secondary">
                            <h5 class='card-title'>Code des Pays</h5>
                        </div>
                        <div class="card-body">
                       
                            <div class="row align-items-baseline pl-5">
                            <?php 
                    //afficher chaque entrée une à une
                    while ($datas_pays = $req_pays->fetch()){
                    ?>
                                <p class="ml-3">Code : <?="$datas_pays[Code_Pays]"?> Pays : <?="$datas_pays[Libelle_Pays]"?> </p>
                                <?php
                    }
                    // Termine le traitement de la requête
                    ?>
                            </div>
                            
                        </div>
                    </div>
                   
                </div>
            </div>
            <!-- CARDS Referentiel Statut -->
            <div class="collapse" id="collapseReferentiel">
                <div class="row row-cols-3">
                    <div class="card border-secondary m-4" min-width="">
                        <div class="card-header bg-transparent border-secondary">
                            <h5 class='card-title'>Code des Statuts</h5>
                        </div>
                        <div class="card-body">
                       
                            <div class="row align-items-baseline pl-5">
                            <?php 
                    //afficher chaque entrée une à une
                    while ($datas_statut = $req_statut->fetch()){
                    ?>
                                <p class="ml-3">Code : <?="$datas_statut[Code_Statut]"?> Statut : <?="$datas_statut[Libelle_Statut]"?> </p>
                                <?php
                    }
                    // Termine le traitement de la requête
                    ?>
                            </div>
                            
                        </div>
                    </div>
                   
                </div>
            </div>
            <!-- CARDS Referentiel Specialite -->
            <div class="collapse" id="collapseReferentiel">
                <div class="row row-cols-3">
                    <div class="card border-secondary m-4" min-width="">
                        <div class="card-header bg-transparent border-secondary">
                            <h5 class='card-title'>Code des Specialités</h5>
                        </div>
                        <div class="card-body">
                       
                            <div class="row align-items-baseline pl-5">
                            <?php 
                    //afficher chaque entrée une à une
                    while ($datas_specialite = $req_specialite->fetch()){
                    ?>
                                <p class="ml-3">Code : <?="$datas_specialite[Specialite_Code]"?> Specialité : <?="$datas_specialite[Libelle_Specialite]"?> </p>
                                <?php
                    }
                    // Termine le traitement de la requête
                    ?>
                            </div>
                            
                        </div>
                    </div>
                   
                </div>
            </div>
    </main>


    <footer>
        <nav class="nav justify-content-center">
            <a class="nav-link active" href="index.php">Les missions</a>
            <a class="nav-link" href="accueilAdmin.php">espace privé</a>
        </nav>
    </footer>

    <!--Bootstrap-->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

    <!--MON JS-->
    <script src="./tp7_top_secret/assets/JS/accueilAdmin.js"></script>
</body>
</html>