<?php
require 'config.php';
require 'accueilAdmin.php';

try {
    // Essaye de se connecter avec PDO
    $connexion = new PDO("mysql:host=localhost;dbname=TP7;port=3306;charset=utf8", $user, $Mdp);
    echo 'connexion DB etablie';
} catch (PDOException $e) {
    // Stop le script et envoie une erreur si la connexion à échoué
    throw new PDOException($e->getMessage(), (int)$e->getCode());
}

$idMissionCode = $_GET["code"];

$deleteMission = $connexion->prepare("DELETE FROM Mission WHERE Mission_Code = :idMissionCode");
$delMission= $deleteMission->bindValue(':idMissionCode', $idMissionCode, PDO::PARAM_INT);
$delMission= $deleteMission->execute();




// header("Location: $url");
?>