START TRANSACTION;

-- ============================================================
--   Suppression et création de la base de données 
-- ============================================================

DROP DATABASE IF EXISTS TP7;
CREATE DATABASE TP7;
USE TP7;


-- ============================================================
--   Création des tables                                
-- ============================================================

CREATE TABLE Pays (
	Code_Pays  INT NOT NULL AUTO_INCREMENT,
	Libelle_Pays VARCHAR (255) NOT NULL,
	CONSTRAINT PK_Pays PRIMARY KEY (Code_Pays)
);

CREATE TABLE Agent(
	Agent_Code	INT NOT NULL AUTO_INCREMENT,
	Agent_Nom VARCHAR(255) NOT NULL,
	Agent_Prenom	VARCHAR(255) NOT NULL,
	Agent_Date_Naiss	DATE NOT NULL,
    Agent_Nationalité	INT NOT NULL,
	CONSTRAINT PK_Agent PRIMARY KEY (Agent_Code),
	CONSTRAINT FK_Agent_Pays FOREIGN KEY (Agent_Nationalité) REFERENCES Pays(Code_Pays)

);

CREATE TABLE Specialite(
	Specialite_Code INT NOT NULL AUTO_INCREMENT,
	Libelle_Specialite VARCHAR(255) NOT NULL,
	CONSTRAINT PK_Specialite PRIMARY KEY (Specialite_Code)
);


CREATE TABLE Agent_Spe (
	Agent_Code INT NOT NULL ,
	Specialite_Code INT NOT NULL,
	Constraint PK_Agent_Spe PRIMARY KEY (Agent_Code),
	CONSTRAINT FK_Agent_spe_Agent FOREIGN KEY (Agent_Code) REFERENCES Agent(Agent_Code),
	CONSTRAINT FK_Agent_Spe_Specialite FOREIGN KEY (Specialite_Code) REFERENCES Specialite(Specialite_Code)

);

CREATE TABLE Statut(
	Code_Statut INT NOT NULL AUTO_INCREMENT,
	Libelle_Statut VARCHAR(255) NOT NULL,
	CONSTRAINT PK_Statut PRIMARY KEY (Code_Statut)
);

CREATE TABLE Contact(
	Contact_Code	INT NOT NULL AUTO_INCREMENT,
	Contact_Nom VARCHAR(255) NOT NULL,
	Contact_Prenom	VARCHAR(255) NOT NULL,
	Contact_Date_Naiss	DATE NOT NULL,
    Contact_Nationalité	INT NOT NULL,
	CONSTRAINT PK_Contact PRIMARY KEY (Contact_Code),
	CONSTRAINT FK_Contact_Pays FOREIGN KEY (Contact_Nationalité) REFERENCES Pays(Code_Pays)

);


CREATE TABLE Cible(
	Cible_Code	INT NOT NULL AUTO_INCREMENT,
	Cible_Nom VARCHAR(255) NOT NULL,
	Cible_Prenom	VARCHAR(255) NOT NULL,
	Cible_Date_Naiss	DATE NOT NULL,
    Cible_Nationalité	INT NOT NULL,
	CONSTRAINT PK_Cible PRIMARY KEY (Cible_Code),
	CONSTRAINT FK_Cible_Pays FOREIGN KEY (Cible_Nationalité) REFERENCES Pays(Code_Pays)

);

CREATE TABLE Type_Planque (
	Type_Planque_Code	INT NOT NULL AUTO_INCREMENT,
	Libelle_Type_Planque VARCHAR (255) NOT NULL,
	CONSTRAINT PK_Type_Planque PRIMARY KEY (Type_Planque_Code)
);

CREATE TABLE Planque(
	Planque_Code	INT NOT NULL AUTO_INCREMENT,
	Planque_Adresse VARCHAR(255) NOT NULL,
    Planque_Pays INT NOT NULL,
    Planque_Type INT NOT NULL,
	CONSTRAINT PK_Planque PRIMARY KEY (Planque_Code),
	CONSTRAINT FK_Planque_Pays FOREIGN KEY (Planque_Pays) REFERENCES Pays(Code_Pays),
    CONSTRAINT FK_Planque_Type_Planque FOREIGN KEY (Planque_Type) REFERENCES Type_Planque(Type_Planque_Code)
);


CREATE TABLE Mission(
	Mission_Code	INT NOT NULL AUTO_INCREMENT,
	Mission_Titre VARCHAR(255) NOT NULL,
	Mission_Descr	VARCHAR(255) NOT NULL,
	Mission_Date_Deb	DATE NOT NULL,
    Mission_Date_Fin	DATE NOT NULL,
    Mission_Pays       INT NOT NULL,
    Mission_Statut     INT NOT NULL,
    Mission_Specialite INT NOT NULL,
	Mission_Type VARCHAR(255) NOT NULL,
	CONSTRAINT PK_Mission PRIMARY KEY (Mission_Code),
	CONSTRAINT FK_Mission_pays FOREIGN KEY (Mission_Pays) REFERENCES Pays(Code_Pays),
    CONSTRAINT FK_Mission_Statut FOREIGN KEY (Mission_Statut) REFERENCES Statut(Code_Statut),
    CONSTRAINT FK_Mission_Specialite FOREIGN KEY (Mission_Specialite) REFERENCES Specialite(Specialite_Code)
	
);


CREATE TABLE Mission_Agent (
	Mission_Code INT NOT NULL,
	Agent_Code INT NOT NULL,
	CONSTRAINT PK_Mission_Agent PRIMARY KEY (Mission_Code),
	CONSTRAINT FK_Mission_Agent_Mission FOREIGN KEY (Mission_Code) REFERENCES Mission(Mission_Code),
	CONSTRAINT FK_Mission_Agent_Agent FOREIGN KEY (Agent_Code) REFERENCES Agent(Agent_Code)
);

CREATE TABLE Mission_Contact (
	Mission_Code INT NOT NULL,
	Contact_Code INT NOT NULL,
	CONSTRAINT PK_Mission_Agent PRIMARY KEY (Mission_Code),
	CONSTRAINT FK_Mission_Contact_Mission FOREIGN KEY (Mission_Code) REFERENCES Mission(Mission_Code),
	CONSTRAINT FK_Mission_Contact_Contact FOREIGN KEY (Contact_Code) REFERENCES Contact(Contact_Code)
);

CREATE TABLE Mission_Cible (
	Mission_Code INT NOT NULL,
	Cible_Code INT NOT NULL,
	CONSTRAINT PK_Mission_Cible PRIMARY KEY (Mission_Code),
	CONSTRAINT FK_Mission_Cible_Mission FOREIGN KEY (Mission_Code) REFERENCES Mission(Mission_Code),
	CONSTRAINT FK_Mission_Cible_Cible FOREIGN KEY (Cible_Code) REFERENCES Cible(Cible_Code)
);

CREATE TABLE Mission_Planques (
	Mission_Code INT NOT NULL,
	Planque_Code	INT NOT NULL,
	CONSTRAINT PK_Mission_Planques PRIMARY KEY (Mission_Code),
	CONSTRAINT FK_Mission_Planques_Mission FOREIGN KEY (Mission_Code) REFERENCES Mission(Mission_Code),
	CONSTRAINT FK_Mission_Planques_Planque FOREIGN KEY (Planque_Code) REFERENCES Planque(Planque_Code)
);





CREATE TABLE Mission_Statut (
	Mission_Code INT NOT NULL,
	Code_Statut INT NOT NULL,
	CONSTRAINT PK_Mission_Statut PRIMARY KEY (Mission_Code),
	CONSTRAINT FK_Mission_Statut_Mission FOREIGN KEY (Mission_Code) REFERENCES Mission(Mission_Code),
	CONSTRAINT FK_Mission_Statut_Statut FOREIGN KEY (Code_Statut) REFERENCES Statut(Code_Statut)
);

CREATE TABLE Administation (
	Admin_Code INT NOT NULL AUTO_INCREMENT,
	Admin_Prenom VARCHAR (255) NOT NULL,
	Admin_nom VARCHAR (255) NOT NULL,
	Admin_Mail VARCHAR (255) NOT NULL,
	Admin_Mdp VARCHAR (255) NOT NULL,
	Admin_Date DATE NOT NULL,
	CONSTRAINT PK_Administration PRIMARY KEY (Admin_Code)

);


INSERT INTO Specialite VALUES (1, 'Enlevement');
INSERT INTO Specialite VALUES (NULL, 'Infiltration');
INSERT INTO Specialite VALUES (NULL, 'Espionnage');
INSERT INTO Specialite VALUES (NULL, 'Sabotage');
INSERT INTO Specialite VALUES (NULL, 'Corps à corps');
INSERT INTO Specialite VALUES (NULL, 'Sniper');
INSERT INTO Specialite VALUES (NULL, 'Explosif');
INSERT INTO Specialite VALUES (NULL, 'Empoisonnement');
INSERT INTO Specialite VALUES (NULL, 'Piratage informatique');


INSERT INTO Type_Planque VALUES (1, 'Maison');
INSERT INTO Type_Planque  VALUES (NULL, 'Appartement');
INSERT INTO Type_Planque VALUES (NULL, 'Studio');
INSERT INTO Type_Planque  VALUES (NULL, 'Hotel');
INSERT INTO Type_Planque  VALUES (NULL, 'Bateau');
INSERT INTO Type_Planque  VALUES (NULL, 'Cabane');
INSERT INTO Type_Planque  VALUES (NULL, 'Usine desafectée');
INSERT INTO Type_Planque  VALUES (NULL, 'Ferme');

INSERT INTO Statut VALUES (1, 'En attente');
INSERT INTO Statut VALUES (NULL, 'En cours');
INSERT INTO Statut VALUES (NULL, 'Echec');
INSERT INTO Statut  VALUES (NULL, 'Abandon');
INSERT INTO Statut  VALUES (NULL, 'Succès');

INSERT INTO Pays VALUES (1, 'France');
INSERT INTO Pays VALUES (NULL, 'Russie');
INSERT INTO Pays VALUES (NULL, 'Chine');
INSERT INTO Pays VALUES (NULL, 'Royaume-Uni');
INSERT INTO Pays VALUES (NULL, 'USA');
INSERT INTO Pays VALUES (NULL, 'Allemagne');
INSERT INTO Pays VALUES (NULL, 'Corée du Nord');
INSERT INTO Pays VALUES (NULL, 'Turquie');
INSERT INTO Pays VALUES (NULL, 'Afrique du Sud');

INSERT INTO Administation VALUES (NULL, 'Mickael','Vandenbulcke', 'mickael.vandenbulcke.simplon@gmail.com', 'bonjour', '2020-09-23');
INSERT INTO Administation VALUES (NULL, 'Mathilde','Vigouroux', 'mathilde.vigouroux.simplon@gmail.com', 'bonjour', '2020-09-23');

COMMIT;
