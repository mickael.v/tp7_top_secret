<?php
require 'config.php';
// require 'accueilAdmin.php';

try {
    // Essaye de se connecter avec PDO
    $connexion = new PDO("mysql:host=localhost;dbname=TP7;port=3306;charset=utf8", $user, $Mdp);
    echo 'connexion DB etablie';
} catch (PDOException $e) {
    // Stop le script et envoie une erreur si la connexion à échoué
    throw new PDOException($e->getMessage(), (int)$e->getCode());
}

$idAgentCode = $_GET["code"];

$deleteAgent = $connexion->prepare("DELETE FROM Agent WHERE Agent_Code = :idAgentCode");
$delAgent= $deleteAgent->bindValue('::idAgentCode', $idAgentCode, PDO::PARAM_INT);
$delAgent= $deleteAgent->execute();




header("Location: $url");
?>