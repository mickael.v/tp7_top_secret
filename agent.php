<?php 
    require 'config.php';

    try {
        // Essaye de se connecter avec PDO
        $connexion = new PDO("mysql:host=localhost;dbname=TP7;port=3306;charset=utf8", $user, $Mdp);
        echo 'connexion DB etablie';
    } catch (PDOException $e) {
        // Stop le script et envoie une erreur si la connexion à échoué
        throw new PDOException($e->getMessage(), (int)$e->getCode());
    }
          
if (isset ($_POST['agentSubmit'])){
    try {
        // je prepare ma requete
        $insert = $connexion->prepare('INSERT INTO Agent (Agent_Nom,Agent_Prenom,Agent_Date_Naiss, Agent_Nationalité) VALUES (?,?,?,?)');
        $insertAgentSpe = $connexion->prepare('INSERT INTO Agent_Spe (Agent_Code,Specialite_Code)VALUES (?,?)');
        //Mes donness
        $Agent_Nom = $_POST['name'];
        $Agent_Prenom = $_POST['firstname'];
        $Agent_Date_Naiss = $_POST['birth'];
        $Specialite_Code = $_POST['specialite'];
        $Agent_Nationalite = $_POST['nationalite'];
        
    
        
    
        $insert->execute(array($Agent_Nom,$Agent_Prenom,$Agent_Date_Naiss,$Agent_Nationalite));
        $Agent_Code = $connexion->lastInsertId();
    
        $insertAgentSpe->execute(array($Agent_Code,$Specialite_Code));
        //succés
    
        echo "insertion OK";
    } catch (PDOException $e) {
        die("pas inséré : " .$e->getMessage());
    }

   
    
};

header("Location: $url");

?>
