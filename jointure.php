<?php
// session_start();
require 'config.php';

try {
    // Essaye de se connecter avec PDO
    $connexion = new PDO("mysql:host=localhost;dbname=TP7;port=3306;charset=utf8", $user, $Mdp);
} catch (PDOException $e) {
    // Stop le script et envoie une erreur si la connexion à échoué
    throw new PDOException($e->getMessage(), (int)$e->getCode());
}




$jointureMissionAgent = $connexion->prepare("SELECT Agent_Nom FROM Mission NATURAL JOIN Mission_Agent NATURAL JOIN Agent");

$jointureMissionAgent->execute(array());

$datas = $jointureMissionAgent->fetch();

$_SESSION['agentnom'] = $datas['Agent_Nom'];

$jointureMissionCible = $connexion->prepare("SELECT Cible_Nom FROM Mission NATURAL JOIN Mission_Cible NATURAL JOIN Cible");

$jointureMissionCible->execute(array());

$datas1 = $jointureMissionCible->fetch();

$_SESSION['cible'] = $datas1['Cible_Nom'];


$jointureMissionContact = $connexion->prepare("SELECT Contact_Nom FROM Mission NATURAL JOIN Mission_Contact NATURAL JOIN Contact");

$jointureMissionContact->execute(array());

$datas2 = $jointureMissionContact->fetch();

$_SESSION['contact'] = $datas2['Contact_Nom'];

$jointureMissionStatut = $connexion->prepare("SELECT Libelle_Statut FROM Mission NATURAL JOIN Mission_Statut NATURAL JOIN Statut");

$jointureMissionStatut->execute(array());

$datas3 = $jointureMissionStatut->fetch();

$_SESSION['statut'] = $datas3['Libelle_Statut'];

$jointureMissionPlanque = $connexion->prepare("SELECT Planque_Adresse FROM Mission NATURAL JOIN Mission_Planques NATURAL JOIN Planque");

$jointureMissionPlanque->execute(array());

$datas4 = $jointureMissionPlanque->fetch();

$_SESSION['planque'] = $datas4['Planque_Adresse'];
