<?php
require 'config.php';
require 'accueilAdmin.php';

try {
    // Essaye de se connecter avec PDO
    $connexion = new PDO("mysql:host=localhost;dbname=TP7;port=3306;charset=utf8", $user, $Mdp);
    echo 'connexion DB etablie';
} catch (PDOException $e) {
    // Stop le script et envoie une erreur si la connexion à échoué
    throw new PDOException($e->getMessage(), (int)$e->getCode());
}

$idPlanqueCode = $_GET["code"];

$deletePlanque = $connexion->prepare("DELETE FROM Planque WHERE Planque_Code = :idPlanqueCode");
$delPanque= $deletePlanque->bindValue(':idPlanqueCode', $idPlanqueCode, PDO::PARAM_INT);
$delPanque= $deletePlanque->execute();




header("Location: $url");
?>